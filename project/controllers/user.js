const Router = require('express').Router;
const UserView = require('../views').UserView;

let router = new Router();

router.route('/user')
    .get(function(req, res){
        UserView.get_all_users(req, res);
    });

module.exports = {
    router
}