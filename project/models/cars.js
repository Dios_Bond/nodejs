const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CarsSchema = new Schema({
    vendor: { type: String, required: true },
    model: { type: String, required: true },
    color: { type: String},
});

let Cars = mongoose.model('Cars', CarsSchema);

return Cars;
