//import mongoose from 'mongoose';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ReferenceSchema = new Schema({
    name: { type: String, required: true },
    value: { type: String, required: true }
});

let Reference = mongoose.model('Reference', ReferenceSchema);

return Reference;
