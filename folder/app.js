//Напишите скрипт, который создает 50 каталогов в указанной директории, в каждом каталоге должен находится файл с содержимым "Файл из каталога №___", после того как каталоги и файлы созданны, срипт должен пройтись по всем созданным каталогам и вывести содержимое файлов в консоль
const fs = require('fs');

function createFolder(path){
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }
    for (let i = 1; i <= 50; i++) {
        let dir = `${path}/${i}`;        
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        fs.appendFile(`${dir}/file.txt`, `Файл из каталога № ${i}`, function (err) {
                if (err) throw err;               
        });        
    }
    fs.readdirSync(path).forEach(folder => {
        if(fs.lstatSync(`${path}/${folder}`).isDirectory()){
            console.log(fs.readFileSync(`${path}/${folder}/file.txt`, "utf8"));
        }
    })
}

createFolder(process.argv[2]); 
