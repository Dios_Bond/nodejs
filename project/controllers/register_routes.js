const UserRoutes = require('./user').router;
const CreateUserRoutes = require('./createuser').router;

//function register_routes(app){
    //app.use(UserRoutes);
    //app.use(CreateUserRoutes);
//}

//module.exports = {
//    register_routes
//}


//import {router as user_router} from './user';
//import { router as courses_router} from './createUser';
//import { check_user_midd } from '../minddlewares';

 function register_routes(base_url, app){
    app.use(base_url, UserRoutes);
    app.use(base_url, CreateUserRoutes);
}


module.exports = {
    register_routes
}
