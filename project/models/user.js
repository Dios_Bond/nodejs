const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new mongoose.Schema({

    name: { type: String, required: true },

    pass: { type: String, required: true },

});

// static method
// UserSchema.statics.create_user()

// hooks
// UserSchema.pre('save', () => {

// })

// UserSchema.post('save', () => {
    
// })

let User = mongoose.model('User', UserSchema);

module.export = User;
