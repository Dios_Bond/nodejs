import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let CourseSchema = new Schema({
    name: { type: String, required: true },
    hours: { type: Number }
});

let Course = mongoose.model('Course', CourseSchema);

export default Course;
